package com.gene.tlonpo.callback;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;

/**
 * @author geneX
 * callback的抽象实现，callback异常发生包装处理
 * 建议使用者实现此类
 */
@Slf4j
public abstract class AbstractCallBack implements CallBack {
    @Override
    public Object callback(Object req) {
        try {
            return doCallBack(req);
        } catch (Exception e) {
            log.warn("This is a exception during callback,req:{}", JSON.toJSONString(req), e);
            //绝对不允许callback抛出异常
            try {
                return exceptionCaught(e, req);
            } catch (Exception xe) {
                return null;
            }
        }
    }

    @Override
    public Object exceptionCaught(Throwable tx, Object req) {
        return null;
    }

    protected abstract Object doCallBack(Object req);

}
