package com.gene.tlonpo.callback;

/**
 * 轮训业务callback顶层接口
 * 具体业务具体实现，需要注册，方便结果回调
 * tips:
 * 后续版版本支持请求返回数据均泛型化
 *
 * @author gene
 */
public interface CallBack {

    /**
     * callback
     * 业务callback，此接口中不建议做耗时的操作
     * 优化点的必要性？callback加入超时控制，增加对此处的关注，异步化之后，callback会成为新的响应时间阻塞点
     * 超时控制在后续版本加入
     *
     * @param req 业务请求数据
     * @return result 业务接口期望返回的结果
     */
    Object callback(Object req);

    /**
     * callback过程中发生异常的兜底方案
     * tlonpo的设计中，callback的异常无法再往上抛了，导致的结果是servlet的AsyncContext无法
     * 完成，导致请求一直阻塞，直到AsyncContext本身超时，这会导致业务不可控,因此加上异常控制
     *
     * @param req 业务请求数据
     * @return 业务接口期望的返回数据
     */
    Object exceptionCaught(Throwable tx, Object req);

}
