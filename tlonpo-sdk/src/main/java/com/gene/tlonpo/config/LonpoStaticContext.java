package com.gene.tlonpo.config;

import com.gene.tlonpo.util.AssertUtil;

import java.util.concurrent.TimeUnit;

/**
 * @author gene
 * 长轮询静态上下文，服务启动的所有信息管理
 */
public class LonpoStaticContext {
    /**
     * redis信息
     */
    static RedisConfig redisConfig;

    /**
     * 服务端口
     */
    static Integer port;

    /**
     * 全局bizKey
     * 选配
     */
    static String globalBizKey;

    /**
     * 全局超时时间
     */
    static Long globalTimeOut = 10L;

    /**
     * 超时时间单位
     * 选配-默认秒
     * 同timeOut
     */
    static TimeUnit globalTimeOutUnit = TimeUnit.SECONDS;

    public static RedisConfig getRedisConfig() {
        return redisConfig;
    }

    public static Integer getPort() {
        return port;
    }

    public boolean hasGlobalBizKey() {
        return !AssertUtil.isEmpty(globalBizKey);
    }

    public static String getGlobalBizKey() {
        return globalBizKey;
    }

    public static Long getGlobalTimeOut() {
        return globalTimeOut;
    }

    public static TimeUnit getGlobalTimeOutUnit() {
        return globalTimeOutUnit;
    }


}
