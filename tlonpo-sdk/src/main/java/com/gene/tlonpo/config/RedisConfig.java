package com.gene.tlonpo.config;

import com.alibaba.fastjson.JSON;

import com.gene.tlonpo.guard.Guard;
import com.gene.tlonpo.guard.GuardDistributedInfo;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Set;

/**
 * reids配置
 *
 * @author gene
 */
public class RedisConfig {
    private static final String KEY_PRE = "TLONPO:PRE:";

    private RedisTemplate<String, GuardDistributedInfo> redisTemplate;

    RedisConfig(RedisTemplate<String, GuardDistributedInfo> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    //put
    public boolean put(String key, GuardDistributedInfo value) {
        redisTemplate.opsForValue().set(key, value);
        return true;
    }

    //删除
    public GuardDistributedInfo get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * 删除指定的分布式
     * @param key
     * @param distributedInfo
     */
    public void sDelete(String key,GuardDistributedInfo distributedInfo) {
        redisTemplate.opsForSet().remove(buildKey(key),distributedInfo);
    }

    /**
     * 添加分布式guard
     *
     * @param key
     * @param distributedInfo
     */
    public void sAdd(String key, GuardDistributedInfo distributedInfo) {
        redisTemplate.opsForSet().add(buildKey(key), distributedInfo);
    }

    /**
     * 获取guard列表
     *
     * @param key
     * @return
     */
    public Set<GuardDistributedInfo> sGet(String key) {
        return redisTemplate.opsForSet().members(buildKey(key));
    }

    private String buildKey(String key) {
        return KEY_PRE + key;
    }

}
