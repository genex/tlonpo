package com.gene.tlonpo.config;

import com.gene.tlonpo.util.AssertUtil;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * @author gene
 * 轮训启动配置
 */
//@Builder
@Getter
@Setter
public class TlonPoInitConfig {

    /**
     * redis配置,直接以来redistemplete
     * 必选配置
     */
    private RedisTemplate redisTemplate;

    /**
     * rockeymq配置
     * 内部监听事件的消费者配置
     * 必选配置
     */
    private TlonpoRocketMqConsumerProperties consumerProperties;

    /**
     * 内部服务端监听的端口
     * 必选配置
     */
    private Integer port;

    /**
     * 全局bizKey
     * 选配
     */
    private String globalBizKey;

    /**
     * 全局超时时间
     * 选配-默认10s
     * 如果不设置全局超时时间，则必须在各个请求中进行超时时间设置
     */
    private Long globalTimeOut = 10L;

    /**
     * 超时时间单位
     * <p>
     * 选配-默认秒
     * 同timeOut
     */
    private TimeUnit globalTimeOutUnit = TimeUnit.SECONDS;

    /*
     * tlonpo 实例id
     * 选填
     * 在一个服务需要有多个tlonpo实例的情况下需要单独设置instanceId用以区分
     * 作废
     */
//    private Integer tlonpoInstanceId = 0;

    /**
     * 初始化操作
     */
    void check() {
        AssertUtil.isNotNull(this.redisTemplate);
        AssertUtil.isNotNull(this.consumerProperties);
        this.consumerProperties.check();
        AssertUtil.isNetPort(this.port);
        AssertUtil.isNotNull(this.globalTimeOut);
        AssertUtil.isNotNull(this.globalTimeOutUnit);
//        AssertUtil.isNotNull(this.tlonpoInstanceId);
    }

}
