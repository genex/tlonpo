package com.gene.tlonpo.config;

import com.gene.tlonpo.util.AssertUtil;
import lombok.Data;

/**
 * @author geneX
 * rocketmq配置
 */
@Data
public class TlonpoRocketMqConsumerProperties {
    private String nameServer;

    private String consumerGroup;

    private String topic;

    private String tag = "*";


    void check() {
        AssertUtil.isNotEmpty(this.nameServer);
        AssertUtil.isNotEmpty(this.consumerGroup);
        AssertUtil.isNotEmpty(this.topic);
        AssertUtil.isNotEmpty(this.tag);
    }


}
