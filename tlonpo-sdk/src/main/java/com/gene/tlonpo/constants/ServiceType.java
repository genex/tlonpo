package com.gene.tlonpo.constants;

/**
 * @author gene
 * 内嵌服务提供类型
 */
public enum ServiceType {
    /**
     * http
     */
    HTTP,

    /**
     * dubbo
     */
    DUBBO

}
