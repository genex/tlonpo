package com.gene.tlonpo.converter;

import java.util.List;

import com.gene.tlonpo.guard.Guard;
import com.gene.tlonpo.guard.GuardDistributedInfo;
import com.gene.tlonpo.guard.GuardRequest;
import com.gene.tlonpo.processor.GuardDecorater;
import com.gene.tlonpo.processor.inter.GuardIDecoraterI;
import com.gene.tlonpo.util.IPUtil;
import com.google.common.collect.Lists;

/**
 * @author gene
 * 哨兵转换器
 */
public class GuardConverter {

    private static final List<GuardIDecoraterI> GUARD_I_DECORATER_I_S = Lists.newArrayList(
        GuardDecorater.GUARD_DECORATER);

    /**
     * guard请求转换为guard
     * 不做数据校验。
     * 提供给sdk内部使用，外部使用自己管理数据校验，报npe
     * case:内部实现暂时使用属性set方式，原则上实体应该使用构造方法直接进行构造
     *
     * @param guardRequest request
     * @return
     */
    public static Guard toGuard(GuardRequest guardRequest) {
        Guard guard = new Guard();
        guard.setAsynContext(guardRequest.getAsynContext());
        guard.setBizKey(guardRequest.getBizKey());
        guard.setCallBack(guardRequest.getCallBack());
        guard.setGuardEventList(guardRequest.getGuardEventList());
        guard.setTimeOut(guardRequest.getTimeOut());
        guard.setTimeOutUnit(guardRequest.getTimeOutUnit());
        guard.setCallBackReq(guardRequest.getCallBackReq());
        //完整性装饰
        GUARD_I_DECORATER_I_S.forEach(de -> de.decorate(guard));
        return guard;
    }

    /**
     * guard转换为分布式模型
     *
     * @param guard
     * @return
     */
    public static GuardDistributedInfo toGuardDistributed(Guard guard) {
        GuardDistributedInfo info = new GuardDistributedInfo();
        info.setBizKey(guard.getBizKey());
        info.setLocalIp(guard.getLocalIp());
        info.setTimeOut(guard.getTimeOut());
        info.setTimeOutUnit(guard.getTimeOutUnit());

        return info;
    }

}
