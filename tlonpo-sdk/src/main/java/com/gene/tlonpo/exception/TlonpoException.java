package com.gene.tlonpo.exception;

/**
 * @author geneX
 * exception
 */
public class TlonpoException extends RuntimeException {
    public TlonpoException(String message) {
        super(message);
    }
}
