package com.gene.tlonpo.guard;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.gene.tlonpo.callback.CallBack;
import com.gene.tlonpo.guard.context.AsynContext;
import com.gene.tlonpo.guard.interf.GuardI;
import com.gene.tlonpo.guard.tool.GuardAggregationTool;
import com.gene.tlonpo.manager.type.GuardManageType;
import com.google.common.collect.Lists;
import lombok.Data;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;

/**
 * @author gene
 * 轮询哨兵，领域模型
 */
@Data
@Slf4j
public class Guard implements GuardI {
    /**
     * 业务key
     * 监听的业务id
     * 和guardId关系为1：n的关系
     */
    private String bizKey;

    /**
     * 本地ip
     * 用作事件发生的定点通知
     */
    private String localIp;

    /**
     * guardId
     * 监听id。和bizkey关系为n：1
     */
    private String guardId;

    /**
     * 超时时间
     * 默认10
     */
    private Long timeOut;

    /**
     * 超时时间单位
     * 默认秒
     */
    protected TimeUnit timeOutUnit;

    /**
     * callback
     * 事件处理callback
     * 接收事件之后的业务处理callback
     */
    private CallBack callBack;

    /**
     * callback的入餐
     */
    private Object callBackReq;

    /**
     * 关心的事件
     * 如果本实例没有注册生效事件，此处赋值将不生效
     */
    private List<String> guardEventList;

    /**
     * 异步上下文
     * 用于异步处理写回数据
     * 和guardId为1：1关系
     */
    private AsynContext asynContext;

    /**
     * 回调并写回数据
     */
    public void callbackAndWrite() {
        Object callbackData = this.callBack.callback(this.getCallBackReq());
        try {
            this.asynContext.write(callbackData);
            GuardAggregationTool.removeGuard(this, GuardManageType.LOCAL);
        } catch (IOException e) {
            log.warn("callback fail", e);
        }
    }

}
