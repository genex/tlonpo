package com.gene.tlonpo.guard;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import com.gene.tlonpo.config.LonpoStaticContext;
import com.gene.tlonpo.guard.interf.GuardI;
import com.gene.tlonpo.service.remote.common.model.DataDto;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author gene
 * 分布式哨兵信息
 * 用于分布式管理哨兵信息，如redis
 */
@Data
@NoArgsConstructor
public class GuardDistributedInfo implements GuardI, Serializable {
    private static final long serialVersionUID = -379840819813789283L;
    /**
     * 业务key
     * 监听的业务id
     * 和guardId关系为1：n的关系
     */
    private String bizKey;

    /**
     * 本地ip
     * 用作事件发生的定点通知
     */
    private String localIp;

    /**
     * 内部通信端口
     * tips:为了满足本机多服务的分布式情况。一个ip:不同端口
     */
    private Integer port = LonpoStaticContext.getPort();

    /**
     * 超时时间
     * 默认10
     */
    private Long timeOut;

    /**
     * 超时时间单位
     * 默认秒
     */
    protected TimeUnit timeOutUnit;

    public GuardDistributedInfo(DataDto dataDto) {
        this.bizKey = dataDto.getBizKey();
        this.localIp = dataDto.getLocalIp();
        this.port = dataDto.getPort();
        this.timeOut = dataDto.getTimeOut();
        this.timeOutUnit = dataDto.getTimeOutUnit();
    }

}
