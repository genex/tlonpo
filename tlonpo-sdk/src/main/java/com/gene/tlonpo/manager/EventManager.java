package com.gene.tlonpo.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author gene
 * 事件，事件由使用者定义
 * 服务启动的时候定义，请求的时候注册关心的事件
 */
public class EventManager {

    /**
     * 是否支持事件
     * 默认不支持
     */
    private static Boolean supportEvent = Boolean.FALSE;

    /**
     * 事件列表
     */
    private static final List<String> EVENT_LIST = new ArrayList<>();

    /**
     * 事件初始化
     *
     * @param eventList 注册的事件列表
     */
    public static void init(List<String> eventList) {
        if (Objects.isNull(eventList) || eventList.size() <= 0) {
            return;
        }

        //不能重复添加
        if (EVENT_LIST.size() > 0) {
            throw new RuntimeException("eventList has init ,please not reinit");
        }

        EVENT_LIST.addAll(eventList);

        supportEvent = Boolean.TRUE;
    }

    /**
     * 是否支持事件
     *
     * @return true/false
     */
    public static boolean isSupport() {
        return supportEvent;
    }

    /**
     * 是否有注册过目标事件
     * null和空默认返回false
     *
     * @param event event
     * @return true/false
     */
    public static boolean hasTargetEvent(String event) {
        if (!isSupport()) {
            throw new RuntimeException("current instance not support longpolling event");
        }

        if (event == null || "".equals(event)) {
            return false;
        }

        return EVENT_LIST.stream().anyMatch(s -> s.equals(event));
    }

    /**
     * 是否有注册过目标事件
     * null和空默认返回""
     *
     * @param event event
     * @return string
     */
    public static String getTargetEvent(String event) {
        if (!isSupport()) {
            throw new RuntimeException("current instance not support longpolling event");
        }

        if (event == null || "".equals(event)) {
            return "";
        }

        return EVENT_LIST.stream().filter(s -> s.equals(event)).findAny().orElse("");
    }

}
