package com.gene.tlonpo.manager.interf;

import java.util.List;

import com.gene.tlonpo.guard.interf.GuardI;

/**
 * @author gene
 * 哨兵管理
 */
public interface GuardManagerI {
    /**
     * 添加哨兵
     *
     * @param guard
     */
    void addGuard(GuardI guard);

    /**
     * 移除哨兵
     * @param guardI
     */
    default void removeGuard(GuardI guardI) {

    }

    /**
     * 获取哨兵
     * 根据bizKey查询guard列表
     * @param bizKey
     * @return
     */
    default <T extends GuardI> List<T> getGuard(String bizKey) {
        return null;
    }

    /**
     * 获取哨兵
     *
     * @param bizKey
     * @param guardId
     * @return
     */
    default <T extends GuardI> T getGuard(String bizKey, String guardId) {
        throw new RuntimeException("can not invoke default method");
    }
}
