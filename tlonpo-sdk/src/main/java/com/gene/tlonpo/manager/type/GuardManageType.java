package com.gene.tlonpo.manager.type;

/**
 * @author gene
 * 哨兵管理器类型
 */
public enum GuardManageType {

    /**
     * 本地
     */
    LOCAL,

    /**
     * 分布式
     */
    DISTRIBUTED,

    /**
     * 超时管理
     */
    TIMEOUT;

}
