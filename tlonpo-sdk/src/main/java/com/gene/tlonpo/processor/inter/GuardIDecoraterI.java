package com.gene.tlonpo.processor.inter;

import com.gene.tlonpo.guard.interf.GuardI;

/**
 * @author gene
 * 哨兵装饰器
 */
public interface GuardIDecoraterI extends Processor {

    /**
     * decorate guardI
     *
     * @param guardI guard interface
     */
    void decorate(final GuardI guardI);
}
