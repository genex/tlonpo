package com.gene.tlonpo.service.handler;

import com.gene.tlonpo.service.remote.common.model.DataDto;
import com.gene.tlonpo.service.remote.common.model.DataRespDto;

/**
 * @author geneX
 * 业务处理handler
 */
public interface BusinessHandler {

    /**
     * 根据业务key处理
     *
     * @param bizKey 业务key
     * @return resp
     */
    DataRespDto localCallback(String bizKey);

    /**
     * 发送远程callback
     *
     * @param dataDto 发送数据
     */
    void remoteCallback(DataDto dataDto);


}
