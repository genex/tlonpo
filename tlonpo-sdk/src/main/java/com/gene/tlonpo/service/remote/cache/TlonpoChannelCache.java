package com.gene.tlonpo.service.remote.cache;

import com.gene.tlonpo.util.AssertUtil;
import io.netty.channel.Channel;
import lombok.EqualsAndHashCode;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author geneX
 * 缓存
 */
public class TlonpoChannelCache {
    private static final ConcurrentHashMap<CacheKey, Channel> CHANNEL_CACHE = new ConcurrentHashMap<>();

    /**
     * 添加channel缓存
     *
     * @param host    host
     * @param post    port
     * @param channel channel
     * @return 返回旧值
     */
    public static Channel add(final String host, final Integer post, final Channel channel) {
        return CHANNEL_CACHE.putIfAbsent(new CacheKey(host, post), channel);
    }

    /**
     * 获取缓存
     *
     * @param host host
     * @param port port
     * @return channel
     */
    public static Channel get(final String host, final Integer port) {
        return CHANNEL_CACHE.get(new CacheKey(host, port));
    }

    /**
     * 移除channel
     *
     * @param host host
     * @param port port
     * @return 当前移除的channel
     */
    public static Channel remove(final String host, final Integer port) {
        return CHANNEL_CACHE.remove(new CacheKey(host, port));
    }


    @EqualsAndHashCode(of = {"host", "port"})
    private static class CacheKey {
        private String host;
        private Integer port;

        CacheKey(String host, Integer port) {
            if (AssertUtil.isEmpty(host) || !AssertUtil.aNetPort(port)) {
                throw new RuntimeException("cache key fail,host or port is null");
            }
            this.host = host;
            this.port = port;
        }
    }


}
