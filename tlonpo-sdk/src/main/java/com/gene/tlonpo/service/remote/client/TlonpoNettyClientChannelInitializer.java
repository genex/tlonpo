package com.gene.tlonpo.service.remote.client;

import com.gene.tlonpo.service.remote.common.handler.TlonpoNettyClientHandler;
import com.gene.tlonpo.service.remote.common.handler.TlonpoNettyDecoder;
import com.gene.tlonpo.service.remote.common.handler.TlonpoNettyEncoder;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

/**
 * @author geneX
 * 客户端组件初始化
 */
public class TlonpoNettyClientChannelInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch){
        //解码
        ch.pipeline().addLast(new TlonpoNettyDecoder());
        //编码
        ch.pipeline().addLast(new TlonpoNettyEncoder());
        //业务处理
        ch.pipeline().addLast(new TlonpoNettyClientHandler());
    }
}
