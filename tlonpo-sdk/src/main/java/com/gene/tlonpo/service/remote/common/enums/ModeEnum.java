package com.gene.tlonpo.service.remote.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author geneX
 * 消息通信模式.
 * request/response
 */
@AllArgsConstructor
@Getter
public enum ModeEnum {
    /**
     * request
     */
    REQUEST((byte) 1),

    /**
     * response
     */
    RESPONSE((byte) 2);
    private Byte mode;

    public static ModeEnum getByMode(byte mode){
        Optional<ModeEnum> first = Arrays.stream(ModeEnum.values()).filter(m -> m.getMode().equals(mode)).findFirst();
        return first.orElse(null);
    }

}
