package com.gene.tlonpo.service.remote.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author geneX
 * 协议版本
 */
@AllArgsConstructor
@Getter
public enum ProtofVersion {
    V1("1.0.0");
    private String versionStr;
}
