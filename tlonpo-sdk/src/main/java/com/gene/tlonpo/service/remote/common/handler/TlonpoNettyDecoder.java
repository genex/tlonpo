package com.gene.tlonpo.service.remote.common.handler;

import com.gene.tlonpo.service.remote.common.protof.TlonpoProtof;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @author geneX
 * 解码器
 */
public class TlonpoNettyDecoder extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        TlonpoProtof decode = TlonpoProtof.decode(in);

        if (decode.getBody() != null) {
            //添加数据,具体数据类型交给对应handler转化
            out.add(decode.getBody());
        }
    }
}
