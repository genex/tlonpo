package com.gene.tlonpo.service.remote.common.model;

import com.gene.tlonpo.config.LonpoStaticContext;
import com.gene.tlonpo.guard.GuardDistributedInfo;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * @author geneX
 * 通信的数据
 */
@Data
@NoArgsConstructor
public class DataDto implements Serializable {
    private static final long serialVersionUID = -379840819813655433L;

    /**
     * 业务key
     * 监听的业务id
     * 和guardId关系为1：n的关系
     */
    private String bizKey;

    /**
     * 本地ip
     * 用作事件发生的定点通知
     */
    private String localIp;

    /**
     * 内部通信端口
     * tips:为了满足本机多服务的分布式情况。一个ip:不同端口
     */
    private Integer port = LonpoStaticContext.getPort();


    /**
     * 超时时间
     * 默认10
     */
    private Long timeOut;

    /**
     * 超时时间单位
     * 默认秒
     */
    protected TimeUnit timeOutUnit;


    public DataDto(GuardDistributedInfo guard) {
        this.bizKey = guard.getBizKey();
        this.localIp = guard.getLocalIp();
        this.port = guard.getPort();
        this.timeOut = guard.getTimeOut();
        this.timeOutUnit = guard.getTimeOutUnit();


    }
}
