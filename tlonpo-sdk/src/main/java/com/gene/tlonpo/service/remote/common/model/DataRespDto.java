package com.gene.tlonpo.service.remote.common.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author geneX
 * 响应协议
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DataRespDto implements Serializable {
    private static final long serialVersionUID = -3798408235655433L;

    /**
     * 错误码
     */
    private int code;

    /**
     * 错误信息
     */
    private String msg;

    /**
     * 请求过来的数据回写，主要用于调用端控制分布式的guard的删除管理
     */
    private DataDto backData;

    public static DataRespDto success(DataDto backData) {
        return result(200, "success",backData);
    }

    public static DataRespDto success(String msg, DataDto backData) {
        return result(200, msg, backData);
    }

    public static DataRespDto fail(int code, String msg) {
        return result(code, msg,null);
    }

    public static DataRespDto fail(String msg) {
        return fail(300, msg);
    }

    private static DataRespDto result(int code, String msg, DataDto backData) {
        return new DataRespDto(code, msg, backData);
    }

    public boolean hasSuccess() {
        return code == 200;
    }


}
