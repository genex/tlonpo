package com.gene.tlonpo.service.remote.common.protof;

import com.gene.tlonpo.service.remote.common.enums.ModeEnum;
import com.gene.tlonpo.service.remote.common.enums.ProtofVersion;
import lombok.Getter;

/**
 * @author geneX
 * 消息协议头
 */
@Getter
public class ProtofHeader {

    /**
     * 版本
     */
    private String version;

    /**
     * 通信模式.请求.响应
     *
     * @see ModeEnum
     */
    private ModeEnum mode;

    //消息类型。如：通知、普通通信等。此次略

    public ProtofHeader(ModeEnum mode) {
        this.version = ProtofVersion.V1.getVersionStr();
        this.mode = mode;
    }

    public ProtofHeader(String version, ModeEnum mode) {
        //判断空校验

        this.version = version;
        this.mode = mode;
    }

    public boolean isReqeust() {
        return ModeEnum.REQUEST == this.mode;
    }
    public boolean isResponse() {
        return ModeEnum.RESPONSE == this.mode;
    }
}
