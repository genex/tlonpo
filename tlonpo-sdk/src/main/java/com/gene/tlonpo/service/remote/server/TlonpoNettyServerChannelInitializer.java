package com.gene.tlonpo.service.remote.server;

import com.gene.tlonpo.service.remote.common.handler.TlonpoNettyDecoder;
import com.gene.tlonpo.service.remote.common.handler.TlonpoNettyEncoder;
import com.gene.tlonpo.service.remote.common.handler.TlonpoNettyServerHanlder;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

/**
 * @author geneX
 * 客户端handler组件初始化器
 */
public class TlonpoNettyServerChannelInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch){
        //解码
        ch.pipeline().addLast(new TlonpoNettyDecoder());
        //编码
        ch.pipeline().addLast(new TlonpoNettyEncoder());
        //业务处理
        ch.pipeline().addLast(new TlonpoNettyServerHanlder());
    }
}
