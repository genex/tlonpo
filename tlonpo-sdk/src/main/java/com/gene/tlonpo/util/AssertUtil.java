package com.gene.tlonpo.util;

import com.gene.tlonpo.exception.TlonpoException;

/**
 * @author geneX
 * assert
 */
public class AssertUtil {


    /**
     * port value
     */
    public static void isNetPort(Integer source) {
        isNotNull(source, "object " + source + " can not be null");
        if (source <= 0) {
            throw new TlonpoException("source port " + source + " should be > 0");
        }

        if (source > (1 << 16) - 1) {
            throw new TlonpoException("source port " + source + " should be < " + ((1 << 16) - 1));
        }

    }

    /**
     * port value
     */
    public static boolean aNetPort(Integer source) {
        return !isNull(source) && source > 0 && source <= (1 << 16) - 1;
    }


    /**
     * notNull
     *
     * @param obj
     */
    public static void isNotNull(Object obj) {
        isNotNull(obj, "object " + obj + " can not be null");
    }

    /**
     * notNull
     *
     * @param obj
     * @param errorMsg
     */
    public static void isNotNull(Object obj, String errorMsg) {
        if (isNull(obj)) {
            throw new TlonpoException(errorMsg);
        }
    }

    /**
     * isNotEmpty
     *
     * @param msg
     */
    public static void isNotEmpty(String msg) {
        isNotEmpty(msg, "object " + msg + " can not be empty");
    }

    /**
     * isNotEmpty
     *
     * @param msg
     * @param errorMsg
     */
    public static void isNotEmpty(String msg, String errorMsg) {
        if (isEmpty(msg)) {
            throw new TlonpoException(errorMsg);
        }
    }


    public static boolean isNull(Object obj) {
        return obj == null;
    }

    public static boolean isEmpty(String msg) {
        return isNull(msg) || "".equals(msg);
    }

    public static void main(String[] args) {
        System.out.println(((1 << 16) - 1));
    }


}
