package com.gene.tlonpo.sdkTest;

import com.gene.tlonpo.config.RedisConfig;
import com.gene.tlonpo.config.TlonPoInitConfig;
import com.gene.tlonpo.config.TlonpoInitializer;
import com.gene.tlonpo.config.TlonpoRocketMqConsumerProperties;
import com.gene.tlonpo.exception.TlonpoException;
import com.gene.tlonpo.service.remote.server.TlonpoNettyServer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.redis.core.RedisTemplate;

import static org.mockito.Mockito.*;

/**
 * @author geneX
 * config test
 */
@RunWith(MockitoJUnitRunner.class)
public class ConfigTest {

    @Test
    public void test_normal() {
        RedisTemplate redisTemplate = spy(RedisTemplate.class);
        TlonpoRocketMqConsumerProperties properties = new TlonpoRocketMqConsumerProperties();
        properties.setNameServer("localhost:67898");
        properties.setConsumerGroup("tlonpo-group");
        properties.setTopic("tlonpo-topic");

        TlonPoInitConfig config = new TlonPoInitConfig();
        config.setConsumerProperties(properties);
        config.setRedisTemplate(redisTemplate);
        config.setPort(1234);

        TlonpoInitializer initializer = new TestTlonpoInitializer(config);
        initializer.init();


    }


    private static class TestTlonpoInitializer extends TlonpoInitializer {

        TestTlonpoInitializer(TlonPoInitConfig config) {
            super(config);
        }

        @Override
        public void init() {
            //内置服务端启动
            startNettyServer();

            //消息监听启动
            startRocketMqListener();

            //初始化静态上下文
            initStaticContext();
        }

        @Override
        protected void startRocketMqListener() {
            try {
                super.startRocketMqListener();
            } catch (TlonpoException te) {

            }

        }

        @Override
        protected TlonpoNettyServer startNettyServer() {
            TlonpoNettyServer tlonpoNettyServer = super.startNettyServer();
            tlonpoNettyServer.close();
            return null;
        }


    }


}
