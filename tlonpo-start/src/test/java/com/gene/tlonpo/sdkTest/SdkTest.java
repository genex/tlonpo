package com.gene.tlonpo.sdkTest;

import com.gene.tlonpo.callback.AbstractCallBack;
import com.gene.tlonpo.guard.GuardRequest;
import com.gene.tlonpo.guard.context.AsynContext;
import org.apache.dubbo.rpc.RpcContext;
import org.junit.Test;

/**
 * @author gene
 */
public class SdkTest {

    @Test
    public void test_guardRequest() {
        GuardRequest request = new GuardRequest();
        request.setBizKey("aaa");
        request.setCallBack(new AbstractCallBack() {
            @Override
            protected Object doCallBack(Object req) {
                return "这是一个testcallback";
            }
        } );
        request.setAsynContext(new AsynContext(RpcContext.startAsync()));
        request.open();

    }


}
